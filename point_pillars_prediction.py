import os
import pickle
from glob import glob
import numpy as np
import tensorflow as tf
from tqdm import tqdm
from processors import SimpleDataGenerator
from inference_utils import generate_bboxes_from_pred, GroundTruthGenerator, focal_loss_checker, rotational_nms
from readers import KittiDataReader
from config import Parameters
from network import build_point_pillar_graph
from kitti import KittiDataset
from precision import get_score
from eval import get_official_eval_result, get_coco_eval_result
from box_np_ops import limit_period, box_lidar_to_camera, center_to_corner_box3d , project_to_image
#from keras.models import load_model
#import tensorflow.keras.models.load_model as load_model
#model = load_model('/home/mcw/Rathi/Project/PointPillars/logs/model.h5')

DATA_ROOT = "/home/mcw/Rathi/kitti_Dataset_val/validation/"
MODEL_ROOT = "./logs"

os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = "0"

def ground_truth_annotations(_kitti_infos):
        #if "annos" not in _kitti_infos[0]:
         #   return None

        gt_annos = [info["annos"] for info in _kitti_infos]

        return gt_annos
'''
def get_start_result_anno():
    annotations = {}
    annotations.update(
        {
            # 'index': None,
            "name": [],
            "truncated": [],
            "occluded": [],
            "alpha": [],
            "bbox": [],
            "dimensions": [],
            "location": [],
            "rotation_y": [],
            "score": [],
        }
    )
    return annotations
def empty_result_anno():
    annotations = {}
    annotations.update(
        {
            "name": np.array([]),
            "truncated": np.array([]),
            "occluded": np.array([]),
            "alpha": np.array([]),
            "bbox": np.zeros([0, 4]),
            "dimensions": np.zeros([0, 3]),
            "location": np.zeros([0, 3]),
            "rotation_y": np.array([]),
            "score": np.array([]),
        }
    )
    return annotations



def convert_detection_to_kitti_annos(self, detection):
        class_names = self._class_names
        det_image_idxes = [k for k in detection.keys()]
        gt_image_idxes = [str(info["image"]["image_idx"]) for info in self._kitti_infos]
        # print(f"det_image_idxes: {det_image_idxes[:10]}")
        # print(f"gt_image_idxes: {gt_image_idxes[:10]}")
        annos = []
        # for i in range(len(detection)):
        for det_idx in gt_image_idxes:
            det = detection[det_idx]
            info = self._kitti_infos[gt_image_idxes.index(det_idx)]
            # info = self._kitti_infos[i]
            calib = info["calib"]
            rect = calib["R0_rect"]
            Trv2c = calib["Tr_velo_to_cam"]
            P2 = calib["P2"]
            final_box_preds = det["box3d_lidar"].detach().cpu().numpy()
            label_preds = det["label_preds"].detach().cpu().numpy()
            scores = det["scores"].detach().cpu().numpy()

            anno = get_start_result_anno()
            num_example = 0

            if final_box_preds.shape[0] != 0:
                final_box_preds[:, -1] = limit_period(
                    final_box_preds[:, -1], offset=0.5, period=np.pi * 2,
                )
                final_box_preds[:, 2] -= final_box_preds[:, 5] / 2

                # aim: x, y, z, w, l, h, r -> -y, -z, x, h, w, l, r
                # (x, y, z, w, l, h r) in lidar -> (x', y', z', l, h, w, r) in camera
                box3d_camera = box_lidar_to_camera(
                    final_box_preds, rect, Trv2c
                )
                camera_box_origin = [0.5, 1.0, 0.5]
                box_corners = center_to_corner_box3d(
                    box3d_camera[:, :3],
                    box3d_camera[:, 3:6],
                    box3d_camera[:, 6],
                    camera_box_origin,
                    axis=1,
                )
                box_corners_in_image = project_to_image(box_corners, P2)
                # box_corners_in_image: [N, 8, 2]
                minxy = np.min(box_corners_in_image, axis=1)
                maxxy = np.max(box_corners_in_image, axis=1)
                bbox = np.concatenate([minxy, maxxy], axis=1)

                for j in range(box3d_camera.shape[0]):
                    image_shape = info["image"]["image_shape"]
                    if bbox[j, 0] > image_shape[1] or bbox[j, 1] > image_shape[0]:
                        continue
                    if bbox[j, 2] < 0 or bbox[j, 3] < 0:
                        continue
                    bbox[j, 2:] = np.minimum(bbox[j, 2:], image_shape[::-1])
                    bbox[j, :2] = np.maximum(bbox[j, :2], [0, 0])
                    anno["bbox"].append(bbox[j])

                    anno["alpha"].append(
                        -np.arctan2(-final_box_preds[j, 1], final_box_preds[j, 0])
                        + box3d_camera[j, 6]
                    )
                    # anno["dimensions"].append(box3d_camera[j, [4, 5, 3]])
                    anno["dimensions"].append(box3d_camera[j, 3:6])
                    anno["location"].append(box3d_camera[j, :3])
                    anno["rotation_y"].append(box3d_camera[j, 6])
                    anno["name"].append(class_names[int(label_preds[j])])
                    anno["truncated"].append(0.0)
                    anno["occluded"].append(0)
                    anno["score"].append(scores[j])

                    num_example += 1

            if num_example != 0:
                anno = {n: np.stack(v) for n, v in anno.items()}
                annos.append(anno)
            else:
                annos.append(empty_result_anno())
            num_example = annos[-1]["name"].shape[0]
            annos[-1]["metadata"] = det["metadata"]
        return annos

def evaluation(gr, detections, output_dir=None):
        """
        detection
        When you want to eval your own dataset, you MUST set correct
        the z axis and box z center.
        """
        gt_annos = ground_truth_annotations()
        dt_annos = convert_detection_to_kitti_annos(detections)
        CLASS_NAMES = [
        "car",
        "pedestrian",
        "bicycle",
        "truck",
        "bus",
        "trailer",
        "construction_vehicle",
        "motorcycle",
        "barrier",
        "traffic_cone",
        "cyclist",
         ]
        # firstly convert standard detection to kitti-format dt annos
        z_axis = 1  # KITTI camera format use y as regular "z" axis.
        z_center = 1.0  # KITTI camera box's center is [0.5, 1, 0.5]
        # for regular raw lidar data, z_axis = 2, z_center = 0.5.

        result_official_dict = get_official_eval_result(
            gt_annos, dt_annos, CLASS_NAMES, z_axis=z_axis, z_center=z_center
        )
        result_coco_dict = get_coco_eval_result(
            gt_annos, dt_annos, CLASS_NAMES, z_axis=z_axis, z_center=z_center
        )

        results = {
            "results": {
                "official": result_official_dict["result"],
                "coco": result_coco_dict["result"],
            },
            "detail": {
                "eval.kitti": {
                    "official": result_official_dict["detail"],
                    "coco": result_coco_dict["detail"],
                }
            },
        }

        return results, dt_annos'''


if __name__ == "__main__":

    params = Parameters()
    '''
    pillar_net = tf.keras.models.load_model(os.path.join(MODEL_ROOT, "model.h5"))
    pillar_net.summary()
    ss
    '''
    pillar_net = build_point_pillar_graph(params)
    pillar_net.load_weights('/home/mcw/Rathi/Project/model.h5')
    #pillar_net.summary()

    data_reader = KittiDataReader()

    lidar_files = sorted(glob(os.path.join(DATA_ROOT, "velodyne", "*.bin")))
    label_files = sorted(glob(os.path.join(DATA_ROOT, "label_2", "*.txt")))
    calibration_files = sorted(glob(os.path.join(DATA_ROOT, "calib", "*.txt")))
    print(len(lidar_files))
    print(len(label_files))
    print(len(calibration_files))
    assert len(lidar_files) == len(label_files) == len(calibration_files), "Input dirs require equal number of files."
    eval_gen = SimpleDataGenerator(data_reader, params.batch_size, lidar_files, label_files, calibration_files)
    print("Length of eval gen", len(eval_gen))

    occupancy, position, size, angle, heading, classification = [], [], [], [], [], []
    for data in tqdm(eval_gen):
    	 o, p, s, a, h, c= pillar_net(data, training=False)
    	 occupancy.append(o)
    	 position.append(p)
    	 size.append(s)
    	 angle.append(a)
    	 heading.append(h)
    	 classification.append(c)
    occupancy = np.concatenate(occupancy, axis=0)
    position = np.concatenate(position, axis=0)
    size = np.concatenate(size, axis=0)
    angle = np.concatenate(angle, axis=0)
    heading = np.concatenate(heading, axis=0)
    classification = np.concatenate(classification, axis=0)
    """print("occupancy  ",occupancy)
    print("position  ",position)
    print("size  ",size)
    print("angle  ",angle)
    print("heading  ",heading)
    print("classification  ",classification)"""
    #occupancy, position, size, angle, heading, classification = pillar_net.predict(eval_gen,
                                                                                  # batch_size=params.batch_size)

     
    set_boxes, confidences = [], []
    loop_range = occupancy.shape[0] if len(occupancy.shape) == 4 else 1
    for i in range(loop_range):
        set_boxes.append(generate_bboxes_from_pred(occupancy[i], position[i], size[i], angle[i], heading[i],
                                                   classification[i], params.anchor_dims, occ_threshold=0.7))
        confidences.append([float(boxes.conf) for boxes in set_boxes[-1]])
    #print("SET BOXES",set_boxes)
    print('Scene 1: Box predictions with occupancy > occ_thr: ', len(set_boxes[0]))

    # NMS
    nms_boxes = rotational_nms(set_boxes, confidences, occ_threshold=0.7, nms_iou_thr=0.5)

    print('Scene 1: Boxes after NMS with iou_thr: ', len(nms_boxes[0]))

    # Do all the further operations on predicted_boxes array, which contains the predicted bounding boxes
    gt_gen = GroundTruthGenerator(data_reader, label_files, calibration_files, network_format=False)
    gt_gen0 = GroundTruthGenerator(data_reader, label_files, calibration_files, network_format=True)
    #print(gt_gen0)
    #print("@@")
    #print(nms_boxes)
    #print
    gt_count=1
    pred_count=1
    scores = []
    score_sum = 0
    for seq_boxes, gt_label, gt0 in zip(nms_boxes, gt_gen, gt_gen0):
        print("---------- New Scenario ---------- ")
        focal_loss_checker(gt0[0], occupancy[0], n_occs=-1)
        '''print("gt_label..",gt_label)
        print("seq_boxes",seq_boxes)
        print(gt0[0])
        print(occupancy[0])'''
        print("---------- ------------ ---------- ")
        gt_list=[]
        pred_list=[]
        for gt in gt_label:
            #print("GT_COUNT",gt_count)
            print(gt)
            gt_list.append(gt.centroid)
            #print(gt.x)
            #print(getattr(gt, "x"))
            #gt_count+=1
            
            
        for pred in seq_boxes:
            #print("PRED COUNT",pred_count)
            print(pred)
            temp =[]
            temp.append(pred.x)
            temp.append(pred.y)
            temp.append(pred.z)
            temp  = np.array(temp)
            pred_list.append(temp)
            #pred_count+=1
        #print("GT LIST",gt_list)
        #print("PRD LIST",pred_list)
        score = get_score(gt_list,pred_list)
        scores.append(score)
    #print(scores)
    print("Calculating metrics..")
    for i in range(0,len(scores)):
        score_sum +=scores[i]
    #print(sum)
    length_scores = len(scores)
    mAP = score_sum/length_scores
    print("mAP",mAP)



    '''for seq_boxes_eval, gt_label_eval, gt0_eval in zip(nms_boxes, gt_gen, gt_gen0):  
         for gt in gt_label:
             print("")
#root_path
#info_path="/home/mcw/Rathi/Kitti_Dataset/kitti_dbinfos_train.pkl"
   




#https://docs.google.com/document/d/1xyWXIo77yJxdnxDRkRkppdf5syzbFm3k-5iOHtV__oc/edit'''
